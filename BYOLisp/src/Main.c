/*
 * Main.c
 *
 *  Created on: Jan 1, 2019
 *      Author: Will MacIntyre
 */

#include <stdio.h>
#include <stdlib.h>
#include <editline/readline.h>
#include "REPL.h"

/* Declare a buffer for user input of size 2048 */


int main(int argc, char** argv) {

	parser_init();

	/* In a never ending loop */
	while (1) {
		/* Output our prompt and get input */
		char* input = readline(prompt);

		/* Add input to history */
		add_history(input);


		/* Attempt to Parse the user Input */
		lval val = parser_parse("<stdin>", input);
		lval_println(val);

		/* Free retrieved input */
		free(input);

	}

	/* Undefine and Delete our Parsers */
	parser_free();
	return 0;
}

