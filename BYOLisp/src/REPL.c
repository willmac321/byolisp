#include <stdio.h>
#include <stdlib.h>
#include <editline/readline.h>
#include "REPL.h"
#include "../../mpc/mpc.h"
#include "PostFixEval.h"

static char* version = "0.0.0.1";

static const char* langD = "MPCA_LANG_DEFAULT";


//statistics are by population
static const char* LANG = "                                                     \
	    number   : /-?[0-9]+(\\.[0-9]+)?/ ;                             \
		helper   : \"max\" | \"min\" ;                  \
		stat	 : \"mean\" | \"median\" | \"mode\" | \"stddev\" | \"var\"; \
	    operator : '+' | '-' | '*' | '/' | '^' | '%' | \"rt\" | <helper> | <stat>;                  \
	    expr     : <number> | '(' <operator> <expr>+ ')';  \
	    lispy    : /^/ <operator> <expr>+ /$/ ;             \
	  ";

static grammar_rules* r;

void parser_init(void){
	/* Print Version and Exit Information */
	printf("Lispy Version %s\n", version);
	puts("Press Ctrl+c to Exit\n");

	r = malloc(sizeof(grammar_rules));
	/* Create Some Parsers */

	r->Number = mpc_new("number");
	r->Helper = mpc_new("helper");
	r->Operator = mpc_new("operator");
	r->Stat = mpc_new("stat");
	r->Expr = mpc_new("expr");
	r->Lispy = mpc_new("lispy");

	mpca_lang(langD, LANG,r->Number, r->Helper, r->Operator, r->Stat, r->Expr, r->Lispy);

}

lval parser_parse(const char* fn, const char* input){
	mpc_result_t res;
	lval rv = lval_err(LERR_BAD_NUM);
	if (mpc_parse(fn, input, r->Lispy, &res)) {
	   /* On Success Print the AST */
	   mpc_ast_print(res.output);
	   rv = evaluate(res.output);
	   mpc_ast_delete(res.output);
	 } else {
	   /* Otherwise Print the Error */
	   mpc_err_print(res.error);
	   mpc_err_delete(res.error);
	 }

	return rv;
}

void parser_free(void){
	   mpc_cleanup(4, r->Number, r->Helper, r->Operator, r->Stat, r->Expr, r->Lispy);
}


