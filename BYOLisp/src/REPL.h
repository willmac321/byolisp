/*
 * REPL.h
 *
 *  Created on: Jan 1, 2019
 *      Author: Will MacIntyre
 */

#ifndef SRC_REPL_H_
#define SRC_REPL_H_

#include "../../mpc/mpc.h"
#include "ValueStructs.h"

typedef struct{
		mpc_parser_t* Number;
		mpc_parser_t* Helper;
		mpc_parser_t* Operator;
		mpc_parser_t* Expr;
		mpc_parser_t* Stat;
		mpc_parser_t* Lispy;
}grammar_rules;

static const char* prompt = "s-->";


void parser_init(void);
lval parser_parse(const char* fn, const char* input);
void parser_free(void);

#endif /* SRC_REPL_H_ */
