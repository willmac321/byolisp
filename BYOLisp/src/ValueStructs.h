/*
 * ValueStructs.h
 *
 *  Created on: Feb 7, 2019
 *      Author: Will MacIntyre
 */

#ifndef SRC_VALUESTRUCTS_H_
#define SRC_VALUESTRUCTS_H_

typedef enum {
	LVAL_NUM,
	LVAL_ERR
} LVAL_TYPE;

typedef enum {
	LERR_DIV_ZERO,
	LERR_BAD_OP,
	LERR_BAD_NUM
} LERR_TYPE;

typedef struct {
	LVAL_TYPE type;
	float num;
	LERR_TYPE err;
	int sigFigs;
} lval;

lval lval_num(float x, int sigFig);

lval lval_num_sig_fig (float x, char* num);

lval lval_err(LERR_TYPE x);

int lval_sigFigs(char* num);

void lval_print(lval v);

void lval_println(lval v);

#endif /* SRC_VALUESTRUCTS_H_ */
