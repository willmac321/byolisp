/*
 * PostFixEval.h
 *
 *  Created on: Dec 31, 2018
 *      Author: Will MacIntyre
 */

#ifndef SRC_POSTFIXEVAL_H_
#define SRC_POSTFIXEVAL_H_

#include "../../mpc/mpc.h"
#include "ValueStructs.h"

int node_number(mpc_ast_t* t);

lval evaluate(mpc_ast_t* t);

lval eval_operator(lval a, char* op, lval b);

lval eval_stat(char* op, mpc_ast_t* t);

lval * create_ordered_array(mpc_ast_t* t, int i);

#endif /* SRC_POSTFIXEVAL_H_ */
