#include <math.h>
#include <float.h>
#include "PostFixEval.h"

int node_number(mpc_ast_t* t){

	int rv = 0;
	if (t->children_num == 0){
		rv = 1;
	}
	else if(t->children_num >= 1){
		rv = 1;
		for (int i = 0; i < t->children_num; i++){
			rv += node_number(t->children[i]);
		}

	}
	return rv;
}

lval evaluate(mpc_ast_t* t){

	if (strstr(t->tag, "number")){
		return lval_num_sig_fig(strtof(t->contents, NULL), t->contents);
	}


	char* op = t->children[1]-> contents;
	char* type = t->children[1]-> tag;
	lval rv = evaluate(t->children[2]);

	int i = 3;
	if (strstr(t->children[i]->tag, "expr") && strstr(type, "stat")){
		rv = eval_stat(op, t);
	}
	while (strstr(t->children[i]->tag, "expr") && !strstr(type, "stat")){

		rv = eval_operator(rv, op, evaluate(t->children[i]));
		i++;
	}

	return rv;
}

lval eval_stat(char* op, mpc_ast_t* t){

	lval rv = lval_err(LERR_BAD_OP);
	int i = 2;

	//get count
	while (strstr(t->children[i]->tag, "expr")){
		i++;
	}

	i = i - 2;

	if (strcmp(op, "mean") == 0){
		rv.type = LVAL_NUM;
		rv.err = -1;
		for(int c = 0; c < i; c++){
			lval test = evaluate(t->children[c + 2]);
			if(test.type == LVAL_NUM) {
				rv.num += test.num;
				rv.sigFigs = test.sigFigs > rv.sigFigs ? test.sigFigs : rv.sigFigs;
			}
			else{
				return test;
			}
		}
		rv.num = rv.num / (i);
	}
	else if (strcmp(op, "median") == 0){
		lval *arr = create_ordered_array(t, i);
//TODO array not right
		for( int k= 0; k < i; k++){
			printf("%f\n", arr[k].num);
		}

		if (i % 2 == 0){
			rv.type = LVAL_NUM;
			rv.err = -1;
			rv = lval_num(0, 0);
			i = (int)(i / 2);
			rv.num = (arr[i].num + arr[i + 1].num) / 2;
//			int sig1 = lval_sigFigs(arr[i]);
//			int sig2 = lval_sigFigsarr()[i + 1]);
//			rv.sigFigs = sig1 > sig2 ? sig1 : sig2;
		}
		else{
			rv.type = LVAL_NUM;
			rv.err = -1;
			rv = lval_num(0, 0);
			i = (int)(i / 2);
			rv.num = arr[i].num;
//			rv.sigFigs = lval_sigFigs(rv.num);
		}
	}
	else if (strcmp(op, "mode") == 0){
		lval *arr = create_ordered_array(t, i);
		int max = 0;
		rv.type = LVAL_NUM;
		rv.err = -1;
		for( int j = 0; j < i; j++){
			int count = 0;
			for( int k = 0; k < i; k++){
				if (arr[j].num == arr[k].num){
					count++;
				}
			}
			if (count > max){
				max = count;
				rv = arr[j];
//				rv.sigFigs = lval_sigFigs(arr[j].num);
			}
		}
	}
	else if (strcmp(op, "stddev") == 0){
		rv.type = LVAL_NUM;
		rv.err = -1;
		lval mean = eval_stat("mean", t);
		lval sum = lval_num(0, 0);
		lval *arr = create_ordered_array(t, i);
		for( int j = 0; j < i; j++){
			lval test = eval_operator(lval_num(arr[j].num - mean.num, 0), "^", lval_num(2, 0));
			if(test.type == LVAL_NUM) {
				sum.num += test.num;
			}
			else{
				return test;
			}

		}
		rv = eval_operator(lval_num(sum.num / i, 0), "rt", lval_num(2, 0));
	}
	else if (strcmp(op, "var") == 0){
		rv.type = LVAL_NUM;
		rv.err = -1;
		rv = eval_stat("stddev", t);
		rv = eval_operator(rv, "^", lval_num(2, 0));
	}

	return rv;
}


lval eval_operator(lval x, char* op, lval y){

	int evalFlag;
	evalFlag = -1;

	if (x.type == LVAL_ERR) { return x; }
	if (y.type == LVAL_ERR) { return y; }

	lval rv = lval_err(LERR_BAD_OP);


	if (strcmp(op, "+") == 0) {
		rv.num = x.num + y.num;
		evalFlag = 1;
	}
	else if (strcmp(op, "-") == 0) {
		rv.num = x.num - y.num;
		evalFlag = 1;
	}
	else if (strcmp(op, "*") == 0) {
		rv.num = x.num * y.num;
		evalFlag = 1;
	}
	else if (strcmp(op, "/") == 0) {
		if(round(y.num) == 0){
			return lval_err(LERR_DIV_ZERO);
		}
		else {
			rv.num = x.num / y.num;
			evalFlag = 1;
		}
	}
	else if (strcmp(op, "^") == 0) {
		rv.num = pow(x.num, y.num);
		evalFlag = 1;
	}
	else if (strcmp(op, "rt") == 0) {
		rv.num = pow(x.num, 1/y.num);
		evalFlag = 1;
	}
	else if (strcmp(op, "%") == 0) {
		rv.num = (int)x.num % (int)y.num;
		evalFlag = 1;
	}
	else if (strcmp(op, "max") == 0){
		if(x.num >= y.num){rv.num = x.num;}
		else{rv.num = y.num;}
		evalFlag = 1;
	}
	else if (strcmp(op, "min") == 0){
		if(x.num <= y.num){rv.num = x.num;}
		else{rv.num = y.num;}
		evalFlag = 1;
	}
	else if (strcmp(op, "mean") == 0){
		rv.num = (x.num + y.num) / 2;
		evalFlag = 1;
	}

	if (evalFlag == 1){
		rv.type = LVAL_NUM;
		rv.err = -1;
		rv.sigFigs = x.sigFigs > y.sigFigs ? x.sigFigs : y.sigFigs;
	}

	return rv;
}



lval * create_ordered_array(mpc_ast_t* t, int i){
	lval *arr = malloc(sizeof(lval) * i);
	for(int c = 0; c < i; c++){
		arr[c] = evaluate(t->children[c + 2]);
	}


	for (int j = 0; j < i; j++){
		for (int k = 0; k < j; k++){
			if (arr[j].num > arr[k].num){
				float n = arr[j].num;
				arr[j].num = arr[k].num;
				arr[k].num = n;
			}
		}
	}

	//TODO why is array not keeping state when passed back up
	for( int k= 0; k < i; k++){
		printf("%f\n", arr[k].num);
	}

	return arr;
}
