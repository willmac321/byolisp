/*
 * ValueStructs.c
 *
 *  Created on: Feb 7, 2019
 *      Author: Will MacIntyre
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ValueStructs.h"

lval lval_num (float x, int sigFig) {
	lval v;
	v.type = LVAL_NUM;
	v.num = x;
	v.sigFigs = sigFig;
	return v;
}

lval lval_num_sig_fig (float x, char* num) {
	lval v;
	v.type = LVAL_NUM;
	v.num = x;
	v.sigFigs = lval_sigFigs(num);
	return v;
}

lval lval_err(LERR_TYPE x) {
  lval v;
  v.type = LVAL_ERR;
  v.err = x;
  return v;
}

/*
 * find the sig figs for rounding
 */
int lval_sigFigs(char* num){


	int rv = 0;
	int len = strlen(num);
	char dec = num[0];
	int dec_count = 0;

	while(dec != '.' && dec_count < len){
		dec_count++;
		dec = num[dec_count];
	}

	if(dec_count < len && dec == '.'){
		dec_count+=1;
	}


	if (dec_count != len){
		for( int i = 0; i < len - dec_count; i++){
			rv++;
		}
	}

//	int whole_num = (int)v;
//	while ( (v - whole_num) > 0){
//		v = 10 * (v - whole_num);
//		whole_num = (int)v;
//		rv++;
//	}


	return rv;
}

/*print lval
 *
 */
void lval_print(lval v) {
	switch (v.type) {
	case LVAL_NUM:
		printf("%.*f",v.sigFigs, v.num);
		break;
	case LVAL_ERR:
		if (v.err == LERR_DIV_ZERO) {
			printf("Error: Division by zero");
		}
		else if (v.err == LERR_BAD_OP) {
			printf("Error: Invalid operator");
		}
		else if (v.err == LERR_BAD_NUM) {
			printf("Error: Invalid Number");
		}
		break;

	}
}

/* print lval followed by newline
 *
 */
void lval_println(lval v) {
	lval_print(v);
	putchar('\n');
}
