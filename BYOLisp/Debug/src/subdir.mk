################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Main.c \
../src/PostFixEval.c \
../src/REPL.c \
../src/ValueStructs.c 

OBJS += \
./src/Main.o \
./src/PostFixEval.o \
./src/REPL.o \
./src/ValueStructs.o 

C_DEPS += \
./src/Main.d \
./src/PostFixEval.d \
./src/REPL.d \
./src/ValueStructs.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/REPL.o: ../src/REPL.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C Compiler'
	gcc -include"C:\Users\Will MacIntyre\Documents\RandomPrograms\BYOLisp\mpc\mpc.c" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"src/REPL.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


